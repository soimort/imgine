# IMGine

Dependencies:

* gcc >= 5
* cmake >= 3.1
* opencv >= 4
* vtk
* boost >= 1.55
* libedit

Compile and run:

    $ mkdir -p build
    $ cd build/
    $ cmake ..
    $ make
    $ ./imgine
